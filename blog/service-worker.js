/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "38a17663138ecd909eb07d6e1542f705"
  },
  {
    "url": "assets/css/0.styles.797dfc43.css",
    "revision": "f79e7b002e54ba46fad2365b135162c8"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/js/10.33b61760.js",
    "revision": "12415271c2a6f6b54b3be0a2ce196eb6"
  },
  {
    "url": "assets/js/11.e936ed72.js",
    "revision": "e1646a86ddf7c5f56f0bd944281aae0c"
  },
  {
    "url": "assets/js/12.06a8905b.js",
    "revision": "1d6abb22b489b3a989e5a709942250dd"
  },
  {
    "url": "assets/js/13.43d57349.js",
    "revision": "38a5dd9b90b3c0da0c20dcd6399f05b1"
  },
  {
    "url": "assets/js/14.9a947fce.js",
    "revision": "d1d4151db12ea4e281f54cdeee3aa673"
  },
  {
    "url": "assets/js/15.00675827.js",
    "revision": "8ec35b3ee51091a57497c4ec3e784edf"
  },
  {
    "url": "assets/js/16.4240201a.js",
    "revision": "95cf6948160175fc14fde7121f36ec47"
  },
  {
    "url": "assets/js/17.4e8e9f2d.js",
    "revision": "b5a0a296e9a6ad70b1bb0cf81ec98bfb"
  },
  {
    "url": "assets/js/18.c0804ecf.js",
    "revision": "a47db15a9d233cf1fd7c263281c3d8be"
  },
  {
    "url": "assets/js/19.e374c696.js",
    "revision": "dad3eb6b9ab2ca350172cb741f11dc90"
  },
  {
    "url": "assets/js/2.553eb4dc.js",
    "revision": "52c31f421f19691793e1f03d2e1ccc3e"
  },
  {
    "url": "assets/js/20.dc1f2462.js",
    "revision": "77a8a1f730054cdd362c60367286dbb3"
  },
  {
    "url": "assets/js/21.2550a5e8.js",
    "revision": "85939d5dbde0d6e42448564b6d5b2d59"
  },
  {
    "url": "assets/js/22.64bef411.js",
    "revision": "b6938648f825d8840abd996742b80b8b"
  },
  {
    "url": "assets/js/23.56de7058.js",
    "revision": "4ab1026322994855e83b060b47df30f7"
  },
  {
    "url": "assets/js/24.7de4d292.js",
    "revision": "47377ebd9ff37e92c2ba3d7932a9ddba"
  },
  {
    "url": "assets/js/25.3483b9cb.js",
    "revision": "ec3bcb37a0c211ac10aaacb13f6bf6f5"
  },
  {
    "url": "assets/js/26.81508f46.js",
    "revision": "f136805b985d41c66a4f188a4722d171"
  },
  {
    "url": "assets/js/27.f884547d.js",
    "revision": "a9082ef344bf346020cda30d27ee3d73"
  },
  {
    "url": "assets/js/28.fc8f3edd.js",
    "revision": "88802a73256e048d0a07bda8cebe5304"
  },
  {
    "url": "assets/js/29.6df73fc7.js",
    "revision": "f8b0ba783c5bb991a2f93365b678bf81"
  },
  {
    "url": "assets/js/3.4c6b6694.js",
    "revision": "039fa398ea98e698feca63912fab412d"
  },
  {
    "url": "assets/js/30.92ddc0da.js",
    "revision": "6436ff64b2dea52889554020b50a8292"
  },
  {
    "url": "assets/js/31.bc595735.js",
    "revision": "97e3abc6378f6a2ef26f43ce43112750"
  },
  {
    "url": "assets/js/32.f6ddf561.js",
    "revision": "2a6eb6ca5090ae2e5eea309f87b848d9"
  },
  {
    "url": "assets/js/33.ba353a20.js",
    "revision": "3ce031566e42745f4261a619457e54b7"
  },
  {
    "url": "assets/js/34.23530c8e.js",
    "revision": "225d0ac26206fa16e70a609caafb3969"
  },
  {
    "url": "assets/js/35.98587bae.js",
    "revision": "4b004e3d9a578570641f3f1e93c2bdf8"
  },
  {
    "url": "assets/js/4.e380c258.js",
    "revision": "20e9610666cd0375585d4a9c15288098"
  },
  {
    "url": "assets/js/5.9b25da44.js",
    "revision": "b301ff144b8b8ddd9051b17fa6a40f78"
  },
  {
    "url": "assets/js/6.3d19bdbc.js",
    "revision": "923dd36f6ddaecaf249ea8a139c91acd"
  },
  {
    "url": "assets/js/7.b8795dc7.js",
    "revision": "774b2e2ffbd548a873fe3a5f9d016887"
  },
  {
    "url": "assets/js/8.9332e1e8.js",
    "revision": "ded71109715612cf04f6565b68d372d2"
  },
  {
    "url": "assets/js/9.875f1e10.js",
    "revision": "728060be589972ed5507e99e4bda98fc"
  },
  {
    "url": "assets/js/app.160d1732.js",
    "revision": "dcf74556c471c8d93526914631a4d681"
  },
  {
    "url": "css/style.css",
    "revision": "9b424e19cb9720524282d326117410bb"
  },
  {
    "url": "docker/index.html",
    "revision": "c5bd7058265c987783c58685714fb47a"
  },
  {
    "url": "go/base/index.html",
    "revision": "a7e56bbda862483054bd51fd523c697d"
  },
  {
    "url": "go/concurrent/index.html",
    "revision": "ecc4113be2905306ca27e8bd793e707c"
  },
  {
    "url": "go/env/index.html",
    "revision": "1104f2826f481fec3645d24fd3f4ebaf"
  },
  {
    "url": "go/gin/index.html",
    "revision": "f83d4921aa2241d366ed16cb2338b13c"
  },
  {
    "url": "go/grammar/index.html",
    "revision": "717f399fcf95de5c228f251d8dd7ac94"
  },
  {
    "url": "go/map/index.html",
    "revision": "6bb2e044e453d6c67e547d21ddb64ffd"
  },
  {
    "url": "go/skills/index.html",
    "revision": "0358386121317e33f2fe48b9d6d94913"
  },
  {
    "url": "go/standard/index.html",
    "revision": "7c6deb58df46fa565797d99bf5892da0"
  },
  {
    "url": "go/struct/index.html",
    "revision": "8d16e9b9257537e31a63d82c84f2bd2e"
  },
  {
    "url": "image/03b0aac181c745ca942dc253d43e3b98.png",
    "revision": "8a5218ea5c8963044858727a9aa68ab8"
  },
  {
    "url": "image/57f156c98cf342f3bb048fa5783bf189.png",
    "revision": "863fcc1f37f0afceb5152448a006e24c"
  },
  {
    "url": "image/76e5770ff3434511b530618d46699d3c.png",
    "revision": "97cd18aa4f66bc308377f24ff4a6048b"
  },
  {
    "url": "image/air.png",
    "revision": "2ffa3ac0342468e7f191a379a2abdc30"
  },
  {
    "url": "image/air1.png",
    "revision": "cfe2142d78d468545c12976c1dcc49a8"
  },
  {
    "url": "image/go01.png",
    "revision": "29bd96350c5f619008101534a321b523"
  },
  {
    "url": "image/go02.png",
    "revision": "8591a5eacde67da217994eaf674ffa48"
  },
  {
    "url": "image/go03.png",
    "revision": "007b94f839e07f01cfee2891dac10a55"
  },
  {
    "url": "image/go04.png",
    "revision": "9825fff0ec6ae07827dbc247c24131a5"
  },
  {
    "url": "image/go05.png",
    "revision": "b79744bb68cb1020fb2869bb2634743e"
  },
  {
    "url": "image/go06.png",
    "revision": "bdacacac2cc9564f6c9d46b99644c869"
  },
  {
    "url": "image/go07.png",
    "revision": "7bc1df07652f787461f4c1a6205b4a5c"
  },
  {
    "url": "image/go08.png",
    "revision": "a7247c732a448996df5e6c10947d18bb"
  },
  {
    "url": "image/go09.png",
    "revision": "a4d86a3d5f6a4ef1f3d281c57cbc0cd4"
  },
  {
    "url": "image/go10.png",
    "revision": "486aa25d14d8f7ffcaebe024f76e570c"
  },
  {
    "url": "image/hangjianju001.png",
    "revision": "9056cb9858e29c810d2233d996ab010f"
  },
  {
    "url": "image/logo.jpg",
    "revision": "a1c7f03e130efb4890ca972c303eab46"
  },
  {
    "url": "image/post01.png",
    "revision": "bd407cab58d27a4cd64534740dff1f56"
  },
  {
    "url": "image/print.png",
    "revision": "6b7882743b342c6dc21f757c1826fa8e"
  },
  {
    "url": "image/print01.png",
    "revision": "56b88a5bdaa045dc7a5517d9676d3fa6"
  },
  {
    "url": "image/setter.png",
    "revision": "bae82564352dab327da57ba679f094b8"
  },
  {
    "url": "image/vue01.png",
    "revision": "ddcf8f1d5979dd5ae5b2290d9096d392"
  },
  {
    "url": "image/vue02.png",
    "revision": "e46676678e55eb99c0a77f11c4ec5fa8"
  },
  {
    "url": "image/vue03.png",
    "revision": "d76110194c2a25d6a0147f23e6af57ab"
  },
  {
    "url": "index.html",
    "revision": "715796e74f21adb3ebb98e92b230182a"
  },
  {
    "url": "js/index.html",
    "revision": "82dbd8be9054b5f7f6752b5176adffa0"
  },
  {
    "url": "mysql/index.html",
    "revision": "5502c76c22058dbc6402a088d176552e"
  },
  {
    "url": "nginx/index.html",
    "revision": "f47fdafba958484e89c6fc52dae55512"
  },
  {
    "url": "php/base/index.html",
    "revision": "7c943352c89665a91aa6e35c251a30e9"
  },
  {
    "url": "php/laravel/index.html",
    "revision": "046d42b9eca6b2bfd008433adb79712c"
  },
  {
    "url": "php/skill/index.html",
    "revision": "4cd94781e4875eb020cbf7ccf4f84f63"
  },
  {
    "url": "print/index.html",
    "revision": "c271f11afa2b502f9d70dee291211d24"
  },
  {
    "url": "python/base/index.html",
    "revision": "4fece3f9eae7d8d8247eeda0094ffd93"
  },
  {
    "url": "python/machine/index.html",
    "revision": "3ef22d5eb5d2bbc835fd1e903d474a51"
  },
  {
    "url": "tool/index.html",
    "revision": "a91d824e190c90b67efc36860a45c02a"
  },
  {
    "url": "vue/assembly/index.html",
    "revision": "76140693ccfa05412ac2f0e5d60186c7"
  },
  {
    "url": "vue/base/index.html",
    "revision": "bfcdc8d915c5774be109e210967b6d35"
  },
  {
    "url": "vue/problem/index.html",
    "revision": "2149e2479432b4fd1ef5eacaffc61768"
  },
  {
    "url": "vue/uniApp/index.html",
    "revision": "277262e2d964e58b07caa94b6e7f2631"
  },
  {
    "url": "web/case/index.html",
    "revision": "939f81c38a3a13afad0a4ba7a6d921af"
  },
  {
    "url": "web/layout/index.html",
    "revision": "d1148172c4a93ccdd5b8867102d1ca92"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
